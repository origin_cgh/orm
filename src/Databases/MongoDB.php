<?php

namespace Origin\Orm\Databases;

use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;

/**
 *
 */
class MongoDB
{
    /** @var Manager */
    private static Manager $connection;

    private function __construct()
    {
        self::$connection = new Manager("mongodb://root:123456@1.15.174.140:27017");
    }

    private function __clone()
    {
    }

    public static function getInstance(): object
    {
        if (empty(self::$connection)) {
            new self();
        }
        return self::$connection;
    }
}